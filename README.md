# Data Science Bowl 2018

This project presents different approaches based on image segmentation experimented from the Data Science Bowl 2018 Kaggle Competition: "_Find the nuclei in divergent images to advance medical discovery_"

## Project composition


The reference of each kernel allows to identify the main characteristics of each approach, according to the following codes:
- __IS__: Image Scaling,
- __SS__: Stage_1 solution,
- __KF__: K-Fold Cross Validation,
- __DA__: Data Augmentation,
- __PP__: Pre-Processing type I(CLAHE)
- __PP2__: Pre-Processing type II(CLAHE),
- __ED__: Extra Data training,
- __KM__: KMean clustering training

## Score Summary

![Scores](/extra/scores.png)